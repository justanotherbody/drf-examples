# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import JsonResponse

from rest_framework import (
    decorators,
    relations,
    serializers,
    viewsets,
)

from .models import Foo


class FooSerializer(serializers.HyperlinkedModelSerializer):
    # Explicitly add the attribute ...
    hello_world = relations.HyperlinkedIdentityField(
        'foo-hello-world',  # see note in hello_world docstring
        source='*',  # pass the model as the argument, instead of an attribute
    )

    class Meta:
        model = Foo
        # and also list it as a field
        fields = ('name', 'url', 'hello_world')


class FooViewSet(viewsets.ModelViewSet):
    queryset = Foo.objects.all()
    serializer_class = FooSerializer

    @decorators.detail_route(methods=['GET'])
    def hello_world(self, request, pk):
        """
        Custom method with access to Foo object.

        NOTICE: Default name appears to be
            <class name.lower()>-<method name.replace('_', '-/)>

        Defined in conjunction with a HyperlinkedIdentityField you can add

        * alternative views of the Foo model e.g., graph
        * RPC entry points for controlling an application

        Linking is discoverable and can save on serialization costs by serving
        only the necessary data.
        """
        model = self.queryset.get(pk=pk)
        return JsonResponse({'msg': 'Hello ' + model.name})
