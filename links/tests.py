# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json

from django.urls import reverse

from django.test import (
    TestCase,
    Client,
)


class TestFoo(TestCase):
    def test(self):
        data = {'name': 'Test FOO'}

        url = reverse('foo-list')
        client = Client()

        resp = client.post(url, data=json.dumps(data), content_type='application/json')
        self.assertEquals(resp.status_code, 201)

        resp_data = resp.json()
        self.assertTrue('name' in resp_data)
        self.assertEqual(resp_data['name'], 'Test FOO')
        self.assertTrue('url' in resp_data)

        # Name is model.lower() + method name.replace('_', '-')
        self.assertTrue('hello_world' in resp_data)

        # Django's reverse doesn't include the server
        hello_url = reverse('foo-hello-world', kwargs={'pk': 1})
        # assert that hello_url is the end of the link
        self.assertTrue(resp_data['hello_world'].endswith(hello_url))
        self.assertTrue(resp_data['hello_world'].startswith('http://'))

        self.assertEqual(200, client.get(hello_url).status_code)
