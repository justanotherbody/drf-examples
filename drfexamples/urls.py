"""drfexamples URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin

from rest_framework.routers import DefaultRouter

from links.views import FooViewSet
from dfgraphs.views import (
    ExampleJobSessionDataDataFrameViewSet,
    JobSessionDataFrameViewSet,
    JobRamView,
)

router = DefaultRouter()
router.register('links', FooViewSet)
router.register('dfgraphs', ExampleJobSessionDataDataFrameViewSet,
                # base_name can be inferred from a queryset, but this ViewSet
                # does not use querysets so we must explicitly provide it
                base_name='dfgraphs')
router.register('job_graphs', JobSessionDataFrameViewSet,
                base_name='job_graphs')

urlpatterns = [
    url(r'^$', JobRamView.as_view()),
    url(r'^admin/', admin.site.urls),
    url(r'^rest/v1/', include(router.urls)),
]
