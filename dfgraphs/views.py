# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os

from bokeh.embed import components
from bokeh.models import ColumnDataSource
from bokeh.models.formatters import NumeralTickFormatter
from bokeh.plotting import figure
from bokeh.resources import CDN
from django.views.generic import TemplateView
import pandas
from pandas_drf_tools.mixins import RetrieveDataFrameMixin
from pandas_drf_tools.serializers import DataFrameRecordsSerializer
from pandas_drf_tools.viewsets import (
    GenericDataFrameViewSet,
    ReadOnlyDataFrameViewSet,
)


# Support 2 API's
#   no args - ExampleJobSessionDataDataFrameViewSet
#   arg     - JobSessionDataFrameViewSet
def get_job_data_df(job_id=None):
    if job_id is None:
        job_id = 170561848

    here = os.path.abspath(os.path.dirname(__file__))
    path = os.path.join(here, 'job-ram-data', str(job_id))

    # Provide default DataFrame that is simply empty
    if not os.path.exists(path):
        # NOTE: this is NOT the complete set of columns - just an example
        return pandas.DataFrame(columns=['ram_gb', 'run_time'])

    df = pandas.read_csv(path)
    df['ram_gb'] = df.pss.apply(lambda kb: kb / 1024 ** 2)
    # Number of seconds the program has been running
    df['run_time'] = df.time - df.time.min()
    return df


class JobRamView(TemplateView):
    """
    Serves a basic bokeh graph with nothing special.
    """
    template_name = 'job-chart.html'

    def get_context_data(self, **kwargs):
        context_data = super(JobRamView, self).get_context_data(**kwargs)

        data_frame = get_job_data_df()
        plot = self.get_ram_plot(data_frame)
        bokeh_script, bokeh_div = components(plot, CDN)

        context_data['title'] = 'Ram usage for Job'
        context_data['bokeh_script'] = bokeh_script
        context_data['bokeh_div'] = bokeh_div

        return context_data

    def get_ram_plot(self, df):
        # Chart type ideas
        # https://bokeh.pydata.org/en/0.12.4/docs/reference/charts.html#horizon
        # https://bokeh.pydata.org/en/0.12.4/docs/reference/charts.html#line
        # https://bokeh.pydata.org/en/0.12.4/docs/reference/charts.html#step
        # https://bokeh.pydata.org/en/0.12.4/docs/reference/charts.html#timeseries
        cds = ColumnDataSource(data=df)  # noqa
        plot = figure(plot_width=800, plot_height=600)
        plot.xaxis.formatter = NumeralTickFormatter(format='0,0')
        # displays the # of seconds without formatting (e.g., mins, secs)
        plot.step(source=cds, x='run_time', y='ram_gb')
        plot.xaxis.axis_label = 'Time (sec)'
        plot.yaxis.axis_label = 'RAM (GB)'
        return plot


class ExampleJobSessionDataDataFrameViewSet(ReadOnlyDataFrameViewSet):
    """
    Serves up a *single* DataFrame as a serializable dataset.

    Demonstrates serving a static dataset via DataFrameRecordsSerializer.

    Effectivley the DataFrame is your entire model collection.

    Returned payload is as follows:

    {
      "columns": ["index", "time", "pid", "user", "pss", "command"],
      "data": [
        [
          0,
          1519288766,
          11308,
          "azimmer",
          5624,
          "python /homes/miker985/code/mem-profiler/meminfo --loop-interval=1"
         ],
         ...  additional rows ...
      ]
    }
    """
    serializer_class = DataFrameRecordsSerializer

    def get_dataframe(self):
        df = get_job_data_df()
        return df


class JobSessionDataFrameViewSet(RetrieveDataFrameMixin,
                                 GenericDataFrameViewSet):
    """
    Serves up the DataFrame associated with our job ID.

    Does not use ReadOnlyDataFrameViewSet because mixins.ListDataFrameMixin is
    not desired (it adds `list()`, which we don't support).

    This class would make an excellent stand-alone endpoint for linking against
    models that had an optional DataFrame associated with each model.

    For models with a mandatory DataFrame it would be simpler to include an
    additional view on the model's regular API ViewSet. That view would closely
    resemble this 3 line method:

    https://github.com/abarto/pandas-drf-tools/blob/master/pandas_drf_tools/mixins.py#L52-L59
    """
    serializer_class = DataFrameRecordsSerializer

    def get_object(self):
        job_id = self.kwargs['index']
        # only job_id 170561848 will return non-empty data
        return get_job_data_df(job_id).iloc[:15]  # truncate to speed up
