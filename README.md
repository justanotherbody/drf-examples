A collection of micro apps demonstrating implementation of Django Rest Framework concepts.

# Apps

## links

Add a hyperlink within a serialized model's output. Allows for linking interesting alternative views of resources e.g., *as graphs* as well as build OOP endpoints on your `rest_framework` viewset instance.

* See links/views.py's `FooSerializer' and `FooViewSet`. Here we add a `hello_world` endpoint to the REST API and include a link to the URI on the serialization of individual resources.

## dfgraphs

Provide interactive graphs supplied by a `pandas.DataFrame` source.

Everything interesting is in `ivews.py`. We define `JobRamView` which serves a `bokeh` graph on the data.
